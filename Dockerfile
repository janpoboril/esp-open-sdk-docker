FROM ubuntu:bionic
ARG VERSION=master

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --quiet \
    make \
    unrar-free \
    autoconf \
    automake \
    libtool-bin \
    gcc \
    g++ \
    gperf \
    flex \
    bison \
    texinfo \
    gawk \
    ncurses-dev \
    libexpat-dev \
    python3-dev \
    python3 \
    python3-serial \
    sed \
    git \
    unzip \
    bash \
    help2man \
    wget \
    bzip2 \
    && rm -rf /var/lib/apt/lists/*

RUN ln -s /usr/bin/python3 /usr/bin/python

RUN useradd --no-create-home espopensdk \
    && mkdir /esp-open-sdk \
    && chown -R espopensdk:espopensdk /esp-open-sdk
USER espopensdk

RUN cd /esp-open-sdk \
    && git clone --recursive --depth 1 --jobs 5 https://github.com/pfalcon/esp-open-sdk.git repo \
    && cd /esp-open-sdk/repo/esptool && git pull origin v2.6 \
    && cd /esp-open-sdk/repo && git submodule foreach rm -rf .git \
    && cd /esp-open-sdk && mv repo/* . \
    && rm -rf repo \
    && make STANDALONE=y || (echo "=== crosstool-NG/build.log:" && cat crosstool-NG/build.log && exit 1)

ENV PATH=/esp-open-sdk/xtensa-lx106-elf/bin:$PATH
